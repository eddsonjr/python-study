'''
    Dicionario e uma estrutura de dados que permite armazenar informacoes no formato 
    chave-valor, sendo as chaves identificadores unicos e, cada chave tendo seu respectivo valor. 
    
    A sintaxe basica de um dicionario no python e:
    
    <dict_name> = {<key1> : <value_key1>, <key2>: <value_key2>, //...}
    
    Dicionarios nao permitem a duplicacao de chaves. Caso existam duas chaves iguais no 
    dicionario, a segunda ira sobreescrever a primeira.
    
'''

#Criando um dicionario simples no qual informa a capital de cada pais

capitalCityDict = {
    'nepal':'Kathmandu',
    'italy': 'Rome',
    'englad': 'London'
}


print(capitalCityDict)


#Voce pode acessar um determinado valor dentro de um dicionario com base em sua chave,
#por exemplo, quero saber qual e a capital da Italia. Isso e feito da seguinte forma:
# <dict_name>[<key>]

print("Capital da Italia: {}".format(capitalCityDict['italy']))



#As chaves e os valores de um dicionario podem ser de tipos de dados diferentes:
numbers = {1: "One", 2: "Two", 3: "Three"}
print(numbers)


#E possivel adicionar um novo elemento a um dicionario, para isso basta seguir:
# <dict_name>[<newKey>] = <newKey_newValue>

numbers[4] = 'Foour' #cria um novo elemento no dicionario
print(numbers)




#Tambem e possivel alterar o valor de uma determinda chave dentro de um dicionario.
#Perceba que a palavra 'Foour' esta errada e vamos concerta-la. Para tanto, basta fazer 
#algo parecido como o caso cima, mas ao inves de usar uma nova chave, vc usa a que ja esta
#criada e altera o valor associado a esta chave

numbers[4] = 'Four' # altera o valor da chave 4 no dicionario
print(numbers)




#E possivel interar entre os elementos de um dicionario atraves de um laco:

for element in numbers:
    print(element)
    
    
print("\n-----------------------------------------------")


'''
    E possivel tambem criar um dicionario atraves da funcao dict(). 
    Tambem e possivel criar um dicionario inicialmente vazio usando a funcao dict() ou 
    somente com {}
'''

MLB_teamDict = dict(
    Colorado='Rockies',
    Boston='Red Sox',
    Minnesota='Twins',
    Milwaukee='Brewers',
    Seattle='Mariners'
)

print(MLB_teamDict)


#Voce pode usar um dicionario para armazenar informacos complexas a respeito de algo.
#Por exemplo, podemos usar um dicionario para armazenar informacoes a respeito de uma pessoa
#Os valores que sao associados a cada chave no dicionario podem ser de diversos tipos:

print("\n-----------------------------------------------")

personDict = dict(
    name='Joe Ramus', #valor tipo string
    age = 34,         #valor tipo int  
    children = {'Ralph',"Joey","Betty"}, #valor tipo set
    pets = {'dog': 'Fido', 'cat': 'Sox'} #outro dicionario
)

print(personDict)
print("Person children: {}".format(personDict['children'])) #pega o nome dos filhoes
print("Person cat pet name: {}".format(personDict['pets']['cat'])) #pega o nome do gato






'''
    Existem algumas operacoes que recaem sobre dicionarios. São elas:
    
    get(<key>) - pega o valor de uma determinada chave
    items() - retorna uma lista de tuplas contendo os pares chave-valor que compoem o dicionario
    key() - retorna a lista de chaves do dicionario
    values() - retorna a lista de valores do dicionario
    pop(<key>) - caso a chave informada exista no dicionario, entao ela e removida e o valor 
                associado a esta chave e retornado. Se a chave nao existir, entao uma exception
                e lancada
                
    update(<obj>) - esta funcao trabalha da seguinte maneira:
        - se a chave nao estiver presente no dicionario, entao um novo objeto e adicionado.
        - se a chave estiver presente, entao o valor correspondente a esta chave e atualizado.
    
    clear() - limpa o dicionario, removendo todos os elementos dele.
'''


print("\n-----------------------------------------------")


#pegando um determinado valor do dicionario de capitais com a funcao get()
print("A capital da italia e: {}".format(capitalCityDict.get("italy")))

#retornando uma lista de tuplas contendo pares chave-valor com a funcao items()
print(capitalCityDict.items())


#pegando a lista de chaves do dicionario com a funcao keys()
print("Chaves do dicionario: {}".format(capitalCityDict.keys()))

#pegando os valores do dicionario com a funcao values()
print("Valores do dicionario: {}".format(capitalCityDict.values()))



#Adicionando um novo objeto ao dicionario
capitalCityDict["brazil"] = "Brasilia"
print(capitalCityDict)

#removendo agora a capital do brasil
print("Removendo a capital do brasil: {}".format(capitalCityDict.pop("brazil")))



#adicionando novos objetos no dicionario
capitalCityDict["brazil"] = "Campinas"
capitalCityDict["russia"] = "Stalingrad"
capitalCityDict["usa"] = "New Jersey"
print(capitalCityDict)


#atualizando o objeto que havia sido adicionado, porem esta com o valor errado. 
capitalCityDict.update(brazil="Brasilia") 
print(capitalCityDict)


#E possivel tambem atualizar mais de um objeto de uma vez, bastando usar o update passando como 
#parametro uma lista de tuplas.
capitalCityDict.update([("russia","Moscow"),("usa","Washignton DC")])
print(capitalCityDict)

#Adicionando um novo elemento com a funcao update()
capitalCityDict.update({"ireland": "Dublin"}) 
print(capitalCityDict)



#Verificando se uma determinada chave faz parte das chaves do dicionario de cidades
isPartOfDict = "ireland" in capitalCityDict.keys()
print("Is found key: {}".format(isPartOfDict))



#Verificando se ha um determiando valor dentro do dicionario
isValueOfDict = "Iceland" in capitalCityDict.values()
print("Is found value: {}".format(isValueOfDict))




