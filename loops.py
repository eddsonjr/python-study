
#while

index1 = 0
while(index1 < 10):
    print(str(index1))
    index1 += 1


print("----------------------------------------- \n")




#Voce pode associar um else a um loop while, assim, quando o while terminar, o else sera executado:
while(index1 < 14):
    print(str(index1))
    index1 += 1
else:
    print("Loop while finalizado!") #sera executado quando o while for finalizado, onde sua condicional passou a ser falsa
    
    
    
    
    
print("----------------------------------------- \n")


#Voce pode tambem ter um loop while infinito, parando-o com o comando 'break':
while(True):
    num = int(input("Informe um numero: "))
    
    #verificando se o numero e par ou impar, se for par, sai do laco
    if(num % 2 == 0):
        print("Saindo...")
        break
    else:
        print("Voce informou um numero impar! \n Vamos continuar....")



print("----------------------------------------- \n")



#E possivel fazer o uso do comando 'continue', forcando uma nova interacao dentro do laco:
index2 = 0
while(index2 < 10):
    index2 += 1
    if(index2 % 2 == 0): 
        continue 
        #forcando a interacao do laco. Perceba que quando se usa o comando 'continue' dentro de um laco, tudo o que 
        #esta apos o continue nao e executado e o laco e interado mais uma vez
    
    print(str(index2))
    








#for 

print("----------------------------------------- \n")
for i in range(0,4):  #equivalente ao for(i = 0; i < 4; i++)
    print(str(i))


