'''
    Para declarar uma variavel no python, voce pode usa a 
    sintaxe: 
    
    <nome_variavel> = <valor>
    
    Nao e permitido declarar o nome de uma variavel iniciando o seu nome com numeros
    
'''
print("---------------------------------------------")
valA = 10
valB = 14.7
str1 = 'Isto e uma string' #strings podem ser declaradas com asplas simples e duplas
str0 = "Isto tambem e uma string"


print("O valor da variavel \'valA\' equivale a: {} e o seu tipo equivale a {}".format(str(valA),type(valA)))
print("O valor da variavel \'valB\' equivale a: {} e o seu tipo equivale a {}".format(str(valB),type(valB)))
print("Primeira string: {}  e seu tipo: {}".format(str0,type(str0)))
print("Primeira string: {}  e seu tipo: {}".format(str1,type(str1)))
print("---------------------------------------------")


#Voce pode ainda instanciar varias variaves em uma unica linha:
a,b,c = 1,2,4
print("Os valores instanciados acima sao {}, {} e {}".format(str(a),str(b),str(c)))
print("---------------------------------------------")



#Concatenacao de strings: 
str3 = "Ola, seja bem vinda "
str4 = "Aly"
print(str3+str4)


