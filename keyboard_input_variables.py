#para ler um valor do teclado, use o comando input

val1 = input("Digite um valor: ")
print("valor lido: " + val1)
print(type(val1)) #informa o tipo de variavel que e val1



'''
    Observe que o python sempre trata os valores lidos via teclado como sendo do 
    tipo string. Para realizar alguns tipos de operacoes com tais valores como, 
    por exemplo, operacoes aritimeticas, e necessario realizar a conversao de tipo.
    
    Isso e feito com funcoes de cast, sendo as mais comuns int() - para converter
    string para valores inteiros, e float() - para converter string para valores 
    decimais. 
    
    
    Importante: no print, sempre que for imprimir uma informacao cujo o tipo
    nao seja string, a mesma deve ser convertida para evitar erro. Isso e feito
    atraves da funcao str().
    
    
'''

print("---------------------------------------------")
int1 = int(input("Insira um valor inteiro: "))
int2 = int(input("Insira um segundo valor inteiro: "))
print("Soma dos numeros acima: " + str((int1+int2)))



print("---------------------------------------------")
int1 = float(input("Insira um valor flutuante: "))
int2 = float(input("Insira um segundo flutuante: "))
print("Soma dos numeros acima: " + str((int1+int2)))



print("---------------------------------------------")
n1 = 12.4
n2 = 16
print(str(n1+n2))





