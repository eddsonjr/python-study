'''
    Funcoes
    
    Uma funcao e um bloco de codigo que realiza uma tarefa especifica. 
    
    Funcoes em python sao definidas como:
    
    
    def <function_name>(<arguments>):
        <function_body>
'''


#Criando uma funcao para somar dois numeros
def sum(a,b):
    print("Soma entre {} e {} = {}".format(a,b,(a+b)))
    

#chamando a funcoa de soma
sum(2,5)



#Criando uam fucnao que retorna a soma de dois valores
def sum2(a,b):
    return a+b


#Chamando a funcao de soma
print("Soma: {}".format(sum2(2,4)))



#Voce pode criar uma funcao cujo um dos argumentos ja possua um valor padrao
def fatorial(value = 1):
    resultado = 1
    for n in range(1,value+1):
        resultado *= n
    
    return resultado


#aqui vamos chamar a funcao fatorial passando o valor que e lido pelo teclado
val = int(input("Informe um valor para calcular sua fatorial: "))
print("{}! = {}".format(val,fatorial(val)))



#Vamos agora calcular a fatorial de 1 - perceba que 1 e o valor padrao do argumento de fatorial, logo, a chamada da funcao pode ser simplesmente
#fatorial()

fatorial1 = fatorial()
print(fatorial1)




print("\n----------------------------------")


'''
    Funcoes lambda
    
    Funcoes lambdas sao tambem conhecidas como funcoes anonimas. 
    
    Todas as características de uma função lambda são muito parecidas com as funções locais, 
    com exceção de duas coisas: elas não possuem uma definição em código, ou seja, são declaradas como variáveis 
    e não possuem um def próprio; e elas são funções de uma linha, que funcionam como se houvesse a instrução return antes do comando.
    
    A sintaxe basica de uma funcao lambda e:
    
    <variavel> = lambda <params> : <body>
    

'''


#Exemplo de uma funcao lambda
soma_lambda = lambda n1,n2: n1+n2
print("A soma entre 5 e 7: {}".format(soma_lambda(5,7)))




'''
    Funcoes como parametros
    
    E possivel passar uma outra funcao como parametro para outra funcao.
    Aqui, para exemplificar, vamos criar uma funcao que recebe 4 parametros, sendo eles: dois numeros, uma string com a operacao e uma outra
    funcao, responsavel por fazer os calculos. Vamos tambem definir algumas outras funcoes cujo o objetivo delas e somente realizar cada operacao
    e retornar o valor

'''

def printf(a,b,operation,otherFunction):
    result = otherFunction(a,b)
    print("{} {} {} = {}".format(a,operation,b,result))
    

def soma(a,b): 
    return a+b


def sub(a,b):
    return a-b


def mult(a,b):
    return a*b


def div(a,b):
    if(b == 0):
        return "Erro: divisao por zero!"

    return a/b


print("\n-------------------------------------------------")
print("Passando funcoes como parametro")
print("---------------------------------------------------")


#Chamando a funcao printf passando para ela as demais funcoes como parametro, alem dos valores
printf(2,6,'+',soma)
printf(5,7,'-',sub)
printf(1.2,2.4,'x',mult)
printf(4,2,'/',div)
printf(1,0,'/',div) #ira gerar um retorno de 'Erro: divisao por zero!'






print("\n-------------------------------------------------")
print("Passando funcoes lambda como parametros")
print("---------------------------------------------------")

#E possivel fazer o mesmo que acima, porem usando funcoes lambdas, compo por exemplo
printf(2.4,2.6,'+', lambda n1,n2: n1+n2)
printf(5,7,'-',lambda n1,n2: n1-n2)
printf(7,7,'x',lambda n1,n2: n1*n2)
printf(49,7,'/',lambda n1,n2: n1/n2) 

'''
    O uso de condicionais em funcoes lambda segue a seguinte ideia:
    
    lambda <arguments> : <Return Value if condition is True> if <condition> else <Return Value if condition is False>

'''

printf(1,0,'/',lambda n1,n2: n1/n2 if(n2 != 0) else 'Erro: Divisao por zero' )

