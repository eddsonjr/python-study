'''
    Em programacao orientada a objetos, heranca e capacidade de uma classe
    herdar informacoes e comportamentos de outra classe. 
    
    
    Em Python, a heranca ocorre da seguinte maneira:
    
    # define a superclass
    class super_class:
        # attributes and method definition

    # inheritance
    class sub_class(super_class):
        # attributes and method of super_class
        # attributes and method of sub_class
    
'''


#Criando um exemplo de superclasse
class Animal:
    
    #construtor
    def __init__(self,nome):
        self.nome = nome
        print("O bichinho {} nasceu!".format(self.nome))
        
    
    #Metodos 
    def comer(self):
        print("O animal {} esta comendo".format(self.nome))
        
    
    def dormir(self):
        print("O animal {} esta dormindo".format(self.nome))
    
    

#Criando agora uma subclasse, que ira herdar os atributos e metodos da super classe Animal
class Cachorro(Animal):
    
    
    #Construtor
    def __init__(self, nome):
        #chama o construtor da super classe passando o nome do animal
        #Note que nome nao e um atributo da classe Cachorro, mas sim da super classe Animal
        super().__init__(nome) 
    
    
    #Metodos
    def latir(self):
        print("{} esta latindo!".format(self.nome))
    
    



#Criando um objeto do tipo cachorro
cachorro = Cachorro("Rex")
cachorro.comer() #perceba qeu comer e um metodo da super classe Animal, nao da subclasse
cachorro.latir() #chamando um metodo da subclasse
cachorro.dormir() #chamando um metodo da super classe
print("\n------------------------------------------------------")

#Verificando se um objeto e de determiando tipo
print("Qual o tipo do objeto cachorro: {} ".format(type(cachorro)))


#Utilizando o isinstance() para verificar se uma instancia de um determinado objeto 
#e de determianda classe

r = isinstance(cachorro,Animal) #verificando se e do tipo animal
print(r) #true, pois cachorro herda de Animal
print("\n------------------------------------------------------")




'''
    Sobrecarga de Metodo
    
    Sobrecarga de metodo significa que vc ira sobrescrever um metodo de uma superclasse
    na sua subclasse.
    
    Em python, a sobreescrita de um metodo se da pelo desenvolvimento de uma nova 
    versao do metodo que esta implementado na superclasse, sem necessidade do uso de 
    alguma palavra reservada. 
    
    Quando se deseja acessar metodos da superclasse, utiliza-se o metodo super(), seguido
    do metodo da superclasse. 

'''


#Criando uma nova classe Gato, que herda de animal e que sobrecarrega o metodo 
#dormir(). Na implementacao da classe, ha o metodo cesta(), que chama o metodo
#comer da superclasse. 


class Gato(Animal):
    
    #construtor
    def __init__(self, nome):
        super().__init__(nome)
        
    
    #sobrecarregando o metodo dormir
    def dormir(self):
       print("O gato {} esta se espriguicando e vai dormir".format(self.nome))
       
       
    #Criando o metodo cesta() que chama o metodo comer() da superclasse
    def cesta(self):
        super().comer()
        print("Depois de comer, o gato irá dormir!")   
    



#Criando um objeto gato
gato = Gato("Pintado")


#perceba que sera chamado a versao sobrecarregada do metodo dormir() e nao 
#a da superclasse
gato.dormir() 



#chamando o metodo cesta()
gato.cesta()
print("\n------------------------------------------------------")
