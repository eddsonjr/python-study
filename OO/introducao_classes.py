'''
    Orientacao a objeto com Python - Classes
    
    Classe - e uma representacao, esquema de um determinado elemento qualquer. 
            Classes podem conter vários tipos de dados (chamados no mundo de OO de
            atributos) e tambem pode realizar varias acoes atraves de funcoes 
            (em orientacao a objeto, uma funcao e chamada de metodo).
            E importante salientar que uma classe serve para representar um 
            determinado elemento específico, armazenando as informacoes a respeito desse 
            elemento.
            
            A classe por si so nao contem nenhum tipo de informacao, nao armazena dados,
            ela e apenas um representacao de um determinado elemento. Para 
            que uma classe ganhe vida e necessario instancia-la, criando assim um 
            objeto desta classe. O objeto em si e o responsavel por manter os dados
            e tambem realizar as acoes propriamente ditas.
         
    Objeto: e a instancia de uma classe, armazenando os dados e realizando as acoes
    propriamente ditas.
    
'''



#Definindo uma classe  - Classe Dog
class Dog: 
    pass 


'''
    Acima criamos uma classe simples, que por hora nao armazena nenhum tipo de dado
    e tambem nao realiza nenhuma acao. 

    No python, para que uma classe receba dados, e necessario que ela implemente
    um metodo de inicializacao. Este metodo e o construtor da classe, responsavel
    por receber os dados inicias de cada instancia da classe e criar o objeto 
    correspondente. Para criar um construtor, temos que criar uma funcao com 
    a nomeclatura __init__() e importante salientar que o primeiro parametro 
    desta funcao e uma variavel chamada self. O construtor sempre e o primeiro
    metodo a ser chamado em uma classe e e executado quando se cria uma instancia
    (objeto) da mesma.
    
    Todas as funcoes dentro de uma classe em Python recebem como primeiro parametro
    o self.
    
    Vamos abaixo criar uma classe Pessoa para representar os dados basicos de um 
    individuo. Em termos de nomeclatura, classes sempre tem a primeira letrao do 
    seu nome escrito em maisculo.
    
    

'''



class Pessoa: 
    
    #construtor
    def __init__(self, nome, idade, sexo):
        #cria um atributo na classe chamado nome, e ja inicializa este atributo 
        #com o valor passado pelo argumento nome
        self.nome = nome
        
        
        #cria um atributo na classe chamado idade e ja inicializa este atributo com 
        #o valor passado pelo argumento idade
        self.idade = idade
        
        
        #cria um atributo na classe chamado sexo e ja inicializa este atributo com 
        #o valor passado pelo argumento sexo
        self.sexo = sexo
    


#Definindo agora um objeto da calsse Pessoa - Criando uma instancia da classe
pessoa = Pessoa('Aly',26,'F')

#Acessando os atributos do objeto pessoa
print("Nome da pessoa: {}".format(pessoa.nome))
print("Idade: {}".format(pessoa.idade))
print("Sexo: {}".format(pessoa.sexo))

#Os prints abaixo nao imprimem as informacoes dos atributos da classe, somente o endereco de memoria delas
#print(pessoa)
#print(str(pessoa))
print("\n----------------------------------------")

'''
    Metodos
    
    Metodos sao funcoes que fazem parte de uma determinada classe. 
    A criacao de um metodo e similar a criacao de uma funcao qualquer, com a diferenca de que 
    sempre devera ser informado o parametro self em sua assinatura.
    
    Vamos criar uma classe carro para demonstrar a criacao de metodos.

'''



class Carro:
    
    def __init__(self,placa,marca,modelo):
        self.placa = placa
        self.marca = marca
        self.modelo = modelo
        
    
    #metodos
    def acelerar(self):
        print("Veiculo acelerando")
       
        
    def frear(self):
        print("Veiculo freando")
    
    
    def ligar(self):
        print("Veiculo dando partida")
        
    


#Criando agora uma instancia de carro e chamando seus metodos
carro = Carro('ATP-001','BMW','X7')
carro.ligar() #chamando um metodo
carro.acelerar() #chamando um metodo
carro.frear() #        