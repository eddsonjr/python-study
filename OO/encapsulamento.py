'''
    Modificadores de acesso e encapuslamento 
    
    https://www.linkedin.com/pulse/modificadores-de-acesso-e-encapsulamento-em-python-vitor-guimar%C3%A3es/?originalSubdomain=pt
    https://www.alura.com.br/apostila-csharp-orientacao-objetos/encapsulamento-e-modificadores-de-acesso
    https://pynative.com/python-encapsulation/
    
    

Na orientação a objetos, esconder os detalhes de implementação de uma
classe é um conceito conhecido como encapsulamento. 
Como os detalhes de implementação da classe estão escondidos, todo o acesso deve 
ser feito através de seus métodos públicos. Não permitimos aos outros saber COMO a 
classe faz o trabalho dela, mostrando apenas O QUÊ ela faz. Para realizar essa ação de 
"esconder o que uma classe faz e quais são seus dados", há o uso de Modificadores de Acesso.

Em Programação Orientada a Objetos, os modificadores de acesso são palavras-chave
que definem a acessibilidade a atributos, métodos e classes. 
Dessa forma, o programador pode definir como esses membros devem ser acessados.

O Python é um pouco diferente das outras linguagens, pois nele tudo é público, não existindo
por tanto palavras reservadas que indicam os modificadores de acesso do tipo private, 
protected, etc. Ao contrário de utilizar palavras chaves para indicar os modificadores de 
acesso, Python utiliza uma técnica chamada de  name mangling, na qual usa um ou dois 
underscores (_,__) para indicar o modificador de acesso, sendo:

    1: Nenhum underscore: atributo/método público - pode ser acessado tanto internamente
                          quanto externamente na classe.
                          
    2: Um underscore (_): Atributo protected - Pode ser acessado somente na classe e em 
                          subclasses.
    
    3: Dois underscores (__): Atributo privado - acessado somente na própria classe. 
                              Para que se tenha acesso externo a classe, é necessário
                              a implementação de getters and setters.
'''





#Vejamos abaixo alguns exemplos de acesso direto e acesso mais restrito


class Conta1: 
    
    #construtor
    def __init__(self,titular,saldo,limite):
        self.titular = titular
        self.saldo = saldo
        self.limite = limite
        


#Criando um objeto conta1
conta1 = Conta1("Edson Jr","1000000000","50000000000")

#acessando os atributos publicos de conta1
#perceba qeu eu posso fazer modificacoes diretamente nos atributos do objeto, sem restricoes
conta1.limite = 100000000
conta1.saldo = 90000000
print("Dados da conta. Titular: {}".format(conta1.titular))
print("limite: {} | saldo: {}".format(conta1.limite,conta1.saldo))
print("\n------------------------------------------------------")






#Criando uma outra classe parecida, porem agora utlizando os atributos
# como privados (name mangling)  e getters e  setters para fazer acesso externo a classe

class Conta2:
    
    #construtor
    def __init__(self,titular,saldo,limite):
        self.__titular = titular
        self.__saldo = saldo
        self.__limite = limite



    #Criando getters and setters
    def get_saldo(self):
        return self.__saldo
    
    def set_saldo(self,saldo):
        self.__saldo = saldo
    
    
    def get_titular(self):
        return self.__titular
    
    def set_titular(self,titular):
        self.__titular = titular
    

    def get_limite(self):
        return self.__limite
    
    def set_limite(self,limite):
        self.__limite = limite



#Criando agora um objeto do tipo conta2

conta2 = Conta2("Aly",100000,10000)

#tentando acessar diretamente os atributos da classe
conta2.__limite = 9 #nao executa - use um set (nao gera erro tambem)


#perceba que as instrucoes abaixo geram erros de aacesso.
#print("Dados da conta. Titular: {}".format(conta2.__titular))
#print("limite: {} | saldo: {}".format(conta2.__limite,conta2.__saldo))


#para evitar o erro de acesso, utiliza-se o getter:
print("Dados da conta: {}".format(conta2.get_titular()))
print("Limite: {} | saldo: {}".format(conta2.get_limite(),conta2.get_saldo()))
print("\n------------------------------------------------------")

#perceba que conta2.__limite = 9 nao e executado. Para atribuir um novo valor 
#a um atributo privado, utilizamos o set:

conta2.set_limite(9)
print("Dados da conta: {}".format(conta2.get_titular()))
print("Limite: {} | saldo: {}".format(conta2.get_limite(),conta2.get_saldo()))
print("\n------------------------------------------------------")



'''
    Apesar do uso de getters and setters, e possivel ainda acessar
    os membros privados de uma classe atraves do name mangling. 
    Para isso, utiliza-se a seguinte sintaxe:
    
    <object_name>._<Class_Name>__<dataMember>

'''


conta3 = Conta2("Sarah",120000000,12000)

#alterando o valor de saldo atraves do name mangling
conta3._Conta2__saldo = 120 #Alterando o valor com o nmae mangling

print("Dados da conta: {}".format(str(conta3._Conta2__titular))) #acessando com name mangling
print("Limite: {} | saldo: {}".format(conta3.get_limite(),conta3.get_saldo()))
print("\n------------------------------------------------------")






'''
    Membros do tipo protected sao acessiveis somente na classe e em suas subclasses.
    Para definir um tipo protegido, utilizase somente um underscore (_).
    
    
    Abaixo um exemplo de acesso protegido e subclasse

'''



#super classe
class Company:
    def __init__(self):
        # Membro protegido
        self._project = "NLP"

# subclasse
class Employee(Company):
    def __init__(self, name):
        self.name = name
        Company.__init__(self)

    def show(self):
        print("Employee name :", self.name)
        #Acessando o membro projetido na classe filha
        print("Working on project :", self._project)

c = Employee("Jessa")
c.show()
print('Project:', c._project)