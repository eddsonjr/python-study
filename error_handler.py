'''
    Em Python usamos a estrutura try / except para tratamento de erros. 
    
    try:
        // codigo que pode causar excecao
    except:
        // codigo para capturar e tratar a excecao
        
        
    Para maiores detalhes, veja a documentacao:
        https://rollbar.com/blog/throwing-exceptions-in-python/#
        

'''

#exemplo de error handler baseado em divisao por zero

try:
    numerator = 10
    denomiator = 0
    
    result = numerator/denomiator
    print("Divisao de 10 por zero: {}".format(result))
except: 
    print("Erro: denominador nao pode ser zero. ")
    



print("\n")


#E possivel tambem pegar e erros especificos 
#Neste caso agora pegando o ZeroDivisionError
try:
    numerator = 10
    denomiator = 0
    
    result = numerator/denomiator
    print("Divisao de 10 por zero: {}".format(result))
except ZeroDivisionError: 
    print("Erro: divisao por zero!")
    
    
    
print("\n")

#Voce tambem pode usar varias clausulas except para variados tipos e erro
try:
    
    even_numbers = [2,4,6,8]
    print(even_numbers[5])

except ZeroDivisionError:
    print("Denominator cannot be 0.")
    
except IndexError:
    print("Index Out of Bound.")




print("\n")

#E possivel executar um bloco final de codigo apos try/except. Para isso usamos o finally. 
#O codigo que esta associado ao finally e executado apos o try/except, independente se houve um
#erro ou nao 

try:
    numerator = 10
    denominator = 0

    result = numerator/denominator

    print(result)
except:
    print("Error: Denominator cannot be 0.")
    
finally:
    print("This is finally block.")
    


print("\n")


'''
    Lancando uma excecao
    
    Caso voce tenha algum codigo que, dado um determinado momento e circunstancia, ele pode 
    disparar uma excecao, voce pode usar a estrutura 
    
    raise Exception() 
    
    para fazer o disparo da excecao
'''



def division(a,b):
    if(b == 0):
        raise Exception("Error: denominator is zero!")
    
    return a/b



#Perceba que se vc fazer a chamada de um metodo que pode disparar uma execao dentro de um bloco 
#try / except, caso a excecao ocorra no seu metodo, ela sera disparada pelo metodo e tratada
#sendo que a execucao do script python continuara. 
#Se ocorrer alguma excecao nao tratada durante a execucao do script python, a execucao do mesmo
#e abortada

try:
    result = division(4,0)  #vai gerar um erro
    print(result)
except Exception as exp:
    print(exp)
    
    


print("\n")



'''
    Criando a sua propria Exception
    
    E possivel criar uma classe que represente uma excecao customizada. Para tanto, esta classe
    deve herdar a classe Exception e em seu construtor, deve-se invocar o construtor da classe mae
    passando uma mensagem de excecao.

'''


class APIException(Exception):


    def __init__(self,httpErrorCode,message="API Error"):
        self.httpErrorCode = httpErrorCode
        self.message = message
        super().__init__(self.message)
        
    

def checkResponse(httpCode,message):
    if( not 200 < httpCode < 299):
        raise APIException(httpCode,message)
    


try:
    httpCode = 404
    message = "Not found!"
    checkResponse(httpCode,message)
except APIException as e:
    print(e)
finally:
    #bloco opcional
    print("closing connection...")
    

    

