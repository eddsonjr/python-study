'''
    O python conta com a estrutura condicional if/else

    No que diz respeito ao if else, python conta com a palavra reservada elif:
    
    if(<cond>):
        //....
    elif(<cond>):
        //....
    else:
        //....
'''


op1 = True
op2 = 2
op3 = 7
op4 = False
myList = ["Aly","Sarah","Gladia","Elijah"] #lista de strings 


if(op1):
    print("Condicional verdadeira")


print("------------------------------------ \n")
    
if(op4):
    print("Condicional verdadeira")
else:
    print("Condicional falsa")
    

print("------------------------------------ \n")


if(op2 > op3):
    print(" 2 > 7")
elif(op4 == True):
    print("Op4 == True")
else:
    print("Nenhuma das anteriores!")
    


print("------------------------------------ \n")


#Agora, querendo saber se um determinado nome informado pelo usuario conta em myList
user = input("Insira o nome de um usuario: ")

if(user in myList): #percebe-se que nao esta sendo feito nenhum tipo de tratamento na comparacao
    print("O usuario encontra-se cadastrado")
else:
    print("O usuario nao esta cadastrado!")
    
