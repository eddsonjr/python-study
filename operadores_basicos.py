#Operadores aritmeticos

soma = 2+3
soma2 = 7.6+0.1
subt = 9.4-7.12
mult = 7*6
div = 12/6
restoDiv = 10 % 3
pot = 3**2


print("-------------------------------------------")
print(f"Somas: {soma} e {soma2}")
print(f"Subtracao: {subt}")
print(f"Multiplicacao: {mult}")
print(f"Divisao: {div}")
print(f"Resto da Divisao: {restoDiv}")
print(f"Potenciacao: {pot}")
print("------------------------------------------- \n")




#Operadores comparativos

comp1 = 2 == 2 
comp2 = 3 != 4
comp3 = 3 == 5

a = 5
b = 7
c = 7

print(comp1)
print(comp2)
print(comp3)
print("{} > {}: {}".format(a,b,(a>b)))
print("{} < {}: {}".format(a,b,(a<b)))
print("{} >= {}: {}".format(c,b,(c>=b)))
print("{} <=  {}: {}".format(c,a,(c<=a)))
print("------------------------------------------- \n")





#Operadores Logicos

x = True
z = False
y = True


l1 = x and z #operador AND
l2 = x and y
l3 = x or z #operador OR
l4 = not(x and y) #operador NOT
l5 = x and not z


print(l1)
print(l2)
print(l3)
print(l4)
print(l5)


