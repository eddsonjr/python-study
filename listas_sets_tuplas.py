
#Criando uma lista bascia com numeros

lista = [0,1,4,3,7,9,2]
print(lista)


'''
    Gerando uma lista de numeros de forma automatica com a funcao range()
    
    Lembre-se que os indices de uma lista sempre comecam pelo numero zero, logo, para se acessar o primeiro elemento 
    da lista acima, voce deve usar lista[0].
    
    range() recebe 3 parametros:
        - start:    em qual numero sera iniciado o range
        - stop:     em qual numero ira parar o range (lembrando que ela trata como n-1, ou seja, se voce colocar 10,
                    o range ira ate 9)
        - step:     os passos que em que ocorrerao o incremento pelo range(). Por padrao sera 1. 
        
    Para criar uma lista,  utilize a sintaxe: 
        [*range(start,stop,step)]
        
    O uso de range(start,stop,step) diretamente, poderá gerar um erro, como por exemplo em listaErro = range(0,9).
    Isso ocorre porque o python não faz a extração dos dados gerados pela funcao range().
    
'''
lista2 = [*range(0,9)]
print(lista2)


listaErro = range(0,9) #gera erro e o print abaixo sai como 'range(0,9)'
print(listaErro)


#Uma outra maneira de criar listas com o range e usando a funcao list() em conjunto com o range()
pares = list(range(0,11,2)) 
print(pares)


impares = list(range(1,11,2))
print(impares)



#Voce tambem pode acessar um determinado elemento de uma lista traves de seu indice
print(f"Acessando o 2o elemento da lista de numeros impares: {impares[1]}")




#Usando um for para percorrer a lista
for element in impares: 
    print(element)
    


print("--------------------------------- \n")



'''
    Matrizes
    
   Uma matriz e uma estrutura de dados com duas dimensoes, sendo linhas e colunas. 
   Python nao possui nenhum tipo de dado built-in para representar matrizes, mas voce pode
   trabalhara com matrizes como sendo uma lista de outra lista.
   
'''


matrix = [
            [1,4,6],
            [3,5,9],
            [12,14,16],
            [17,19,21]
          ]
    

print(matrix)

print("\n")


#imprimindo a lista acima no formato de linhas e colunas
for i in matrix:
    print(i)


print("\n")


#Voce tambem pode percorrer uma matriz pegando cada elemento
#individualmente, utilizando dois lacos for, sendo um para percorrer as linhas e outro
#para percorrer as colunas:


for line in range(0,4):
    for col in range(0,3):
        print(matrix[line][col])
        


print("\n")


#Usando a tecnica acima, vamos supor que precisamos criar uma nova matriz com os valores da anteriore multiplicados por 2


multpMatrix = []

for line in range(0,4):
    lines = []
    for col in range(0,3):
        multipled = matrix[line][col] * 2
        lines.append(multipled)
    
    multpMatrix.append(lines)
    


#agora imprimindo a matriz com os valores multiplicados
for m in multpMatrix:
    print(m)
    



print("--------------------------------- \n")


'''
    Em Python, voce pode ter criar listas com elementos dos mais variados tipos, ou seja, sua lista
    pode ter numeros inteiros, flutuantes, strings, outras listas, tuplas, sets, etc.
    
    No entanto, e necessario cuidado ao utilizar esta flexibilidade que o python permite, pois dependendo 
    do tipo de operacao que voce ira realizar sobre esta lista, falhas e erros poderao ocorrer. Para contornar
    tal problema, algum tipo de verificacao podera ser necessaria sob cada elemento da lista 
    
'''


heterogeneList = [1,2.4,"Testando",[6,7,8]]
print(heterogeneList)



for i in heterogeneList:
    print(i)



'''
    Metodos aplicados em listas
    
    Aqui estao alguns metodos mais comuns aplicados em listas no python
    
    append() - adiciona um elemento no final da lista.
    extend() - Adiciona mais de um elemento ao final da lista
    insert() - Adiciona um elemento a lista levando em consideracao uma posicao dada
    remove() - Remove um elemento da lista. Obs: se houver mais de um mesmo elemento na lista, somente a primeira 
                ocorrencia ira ser removida
    pop() - Remove um elmento da lista levando em consideracao uma posicao dada
    reverse() - Reverte a ordem dos elementos de uma lista. Este metodo modifica a lista original.
    len() - imprime o tamanho da lista
    min() - retorna o menor valor da lista
    max() - retorna o maior valor da lista
    count() - retorna o numero de ocorrencias de um determinado elemento na lista
    sort() - ordena a lista em ordem crescente. O metodo sort() funciona apenas com listas homogeneas, ou seja,
            listas cujos os itens sao do mesmo tipo.
'''


print("\n----------------------------------")

originalList = [3,4,2,0]
print("Original list: {}".format(originalList))


#Mostrando o tamanho da lista com len()
print("Tamanho da lista: {}".format(len(originalList)))

#Usando append() para adicionar um elemento ao final da lista - Perceba que a lista original e alterada
originalList.append(9) 
print("Appended 9: {}".format(originalList))



#Usando extend() para adiconar mais de um elemento ao final da lista - Perceba que a lista original e alterada
originalList.extend([11,7,1])
print("Extended list: {}".format(originalList))



#Usando o instert() para adicionar um elemento levando em consideracao uma dada posicao - Perceba que altera a lista 
originalList.insert(0,14)  #adiciona o numero 14 a primeira posicao da lista
print("Inserted list: {}".format(originalList))




#Usando agora o remove() para remover um elemento da lista. Perceba que se houver mais de um mesmo elemento repetido
#e este elemento estiver sendo removido, somente a primeira ocorrencia deste elemento na lista sera removida
#para exemplificar, vamos adicionar um outro elemento 14 a lista 
originalList.append(14) #adiciona um elemento ao final da lista - este elemento esta repetido na lista
print("Appended 14: {}".format(originalList))
originalList.remove(14) #removendo o numero 14 da lista - somente a primeira ocorrencia do 14 sera removida
print("Removed the element number 14: {}".format(originalList))



#Usando pop() para remover um elemento da lista levando em consideracao sua posicao
originalList.pop(0) #removendo o elemento que esta na posicao 0 da lista (primeiro elemento)
print("Pop first element: {}".format(originalList))



#Ordenando a lista com sort() - Perceba que altera a lista original
originalList.sort()
print("Sorted List: {}".format(originalList))




#Ordenando a lista ao inverso com o reverse() - Perceba que altera a lista original
originalList.reverse()
print("Reversed list: {}".format(originalList))



#Descobrindo o maior e o menor elemento de uma lista com max() e min()
print("Min and Max values in the list: {} / {}".format(min(originalList), max(originalList)))



#descobrindo a quantidade de ocorrencias de um determinado elemento dentro de uma lista com count()
print("How many times the 14 elment occurs inside list: {}".format(originalList.count(14)))


print("\n----------------------------------")


#Operacoes parecidas podem ser feitas com listas contendo inteiramente strings, a saber:
stringList = ["Sarah","Fernanda","Olesia","Blake","Ana","Laize"]
print("List of names: {}".format(stringList))



#Ordenacao - Ordena de forma alfabetica
stringList.sort()
print("Sorted: {}".format(stringList))


#Reversa - Ordena de forma inversamente alfabetica
stringList.reverse()
print("Reversed: {}".format(stringList))



#Max e Min  - leva em consideracao as letras iniciais.
print("First / Last name: {} / {}".format(min(stringList),max(stringList)))



print("\n----------------------------------")


'''
    Existem tambem algumas outras operacoes relacionadas a manipulacao de listas, tais como:
    
    -slice - usado para capturar uma parte de uma lista
    -concatenate - Usado para realizar uma concatenacao dos elementos de uma lista com os elementos de uma 
                    outra lista
    -multiply - Multiplica (repete os elementos) daquela determinada lista uma determinada quantidade N de vezes
    -index() - retorna a posicao onde a primeira ocorrencia de um determinado elemento foi encontrado
    
    Vejamos:

'''


#Slice
print("Original list: {}".format(stringList))
print("Slicing the list of names: ")
print("Slice [:4] > {}".format(stringList[:4])) #imprime do primeiro ate o 4o elemento da lista
print("Slice [2:] > {}".format(stringList[2:])) #imrpime iniciando da posicao 2 e indo ate o final da lista
print("Slice [2:4] > {}".format(stringList[2:4])) #imprime iniciando da posicao 2 e vai ate posicao 4
print("Slice [:] > {}".format(stringList[:])) #imprime a lista inteira


print("\n")
print("Multiplying the list 2 times: ")
print(stringList*2) #repete todos os elementos da lista 2 vezes



#encontrando a primeira ocorrencia do nome Fernanda na lista
print("What is the first ocurrency of \"Fernanda\" name:  {}".format(stringList.index("Fernanda")))


#concatenar uma lista significa basicamente adicionar 'somar' listas diferentes, ou seja, 
#unir os elementos de uma lista com os elementos de outra lista
#Aqui, vamos usar a operacao de concatenacao para unir a lista de nomes com a de numeros

concatenatedList = stringList + originalList
print("Lista concatenada: {}".format(concatenatedList))

#E importante lembrar que em python, uma lista pode ter varios tipos de dados







# SET 

'''
    Em Python, SET e considerado uma colecao de objetos. OS sets possuem as seguintes caracteristicas:
    
    1. Eles nao sao ordenados
    2. Elementos sao unicos. Se houve mais de um mesmo elemento no set, somente um sera representado os demais iguais ignorados
    3. Os elementos num set sao do tipo imutavel, porem o set em si pode ser alterado atraves de algumas operacoes, tais como:
        uniao e intersecao. 
        
        
    Os sets sao definidos atraves de {}, seguindo a sintaxe:
    
    myset = { //.. elementos } (nao confunda set com dicionario)
    
    ou 
    
    mySet = set([ //... elementos ]) ou mySet = (( <tupla> ))
    
    Vejamos alguns exemplos de sets: 


'''


print("\n-------------------------------------------")
print("SETs")
print("---------------------------------------------")
#Criando um set

mySet = {1,4,2,0}
print(mySet) #perceba que na impressao, a ordem pode nao ser respeitada


myStringSet = set(["Flora","Sarah"])
print(myStringSet) 


#Pegando o tamanho de um set
print("Tamanho do set {}: {}".format(mySet,len(mySet)))




#Percorrendo os elementos de um set com laco for
print("Percorrendo os elementos do set com laco for:")
for element in mySet:
    print(element)


'''

    As operacoes basicas sobre set incluem:
    
    -add(): Adiciona um elemento a um set
    -remove(): Remove um elemento de um set. Se o elemento nao estiver no set, sera disparado um exception
    -discard(): Parecido como o remove(), também serve para remover um elemento do set, mas ao contrário do remove(), se o elemento a ser removido nao
                esta no set, nao e disparado nenhuma exception
    
    -pop(): remove randomicamente um elemento qualquer (randomicamente) do set
    
    
'''



#Adicionando um elemento em um set
mySet.add(14)
print("Set after add 14 element: {}".format(mySet))


#Removendo um elemento com o remove()
mySet.remove(14)
print("Set after remove the 14 element: {}".format(mySet))


#Tentando remove novamente o elemento 14 do set usando o remove() - Perceba que ira gerar um erro
#mySet.remove(14)
#print("Set after remove the 14 element again: {}".format(mySet)) # nao sera impresso pois vai gerar um erro


#removendo um elemento qualquer do set com o pop - remocao randomica
mySet.pop()
print("Set after poped any value: {}".format(mySet))




#Tuples

print("\n-------------------------------------------")
print("Tuplas")
print("---------------------------------------------")

'''
    Tuplas sao bem parecidas com as listas, mas com a diferenca de serem imutaveis. 
    A criacao de uma tupla e feita colocando os elementos dentro de (), separando esses elementos por virgula, podendo
    ter qualquer tamanho e os elementos que compoem esta tupla podem ser de diferentes tipos.
    
'''



#Criando tuplas
myTuple = ("posx",2.5,"posy",4.5)
myTuple2 = ((1.2,5.6),[1.0,2.0,4.0])
print(myTuple)
print(myTuple2)


#Acessando os elementos de cada tupla
print("Last element of tuple: {}".format(myTuple[3]))
print("The first position: {}".format(myTuple2[0]))




#perceba que em myTuple2, o primeiro elemento e uma outra tupla. Para acessar individualmente, podemos fazer:
posX = myTuple2[0][0]
posY = myTuple2[0][1]
print("As posicoes X e Y iniciais sao: {} e {}".format(posX,posY))
#Algo parecido pode ser feito para acessar individualmente os elementos da segunda posicao de myTuple2, uma vez que esse objeto e uma lista



#Verificando se um determinado elemento esta dentro deu ma tupla
print("O valor 2.5 esta dentro de myTuple? {}".format(2.5 in myTuple))




#percorrendo os elementos da tupla com set
print("Percorrendo os elementos do set com laco for")
for element in mySet:
    print(element)
    
    


